# terraform_learning_plans

Code for the Morpheus Terraform learning plans. 

## Getting started

In order to utilize this repository for your learning plan, you must select study level you want to work with. Branches are named for the study level.

### Available Branches

- 101

--------------------------------------------------------------------------------------------------------------------------------------------------------
***101***

This branch is the base level learning plan to learn how to use Terraform with Morpheus.

***Morpheus Knowledge Prerequisites***
-   Spec Templates
-   Blueprints
-   Instance Types
-   Tasks
-   Workflows
-   Terraform Basics
-   Morpheus Variables

***Required Capabilities***
- AWS Account
- IAM Account with requisite Morpheus permissions
- AWS IAM Account Access Key
- AWS IAM Account Secret Key
- Morpheus Appliance version 5.4.0 or later

